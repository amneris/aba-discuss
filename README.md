# ABA Discuss based on Meteor and Telescope.JS.

1. Install [Meteor](http://meteor.com)
2. Install ABA Package:
```
meteor add aba
```
3. Run Meteor server:
```
meteor run --settings settings.json
```

## Folder Conventions 

* Any code that’s placed within a folder named “clients” will only run on the client. Like the 'isClient' conditional. 
* Any code that’s placed within a folder named “server” will only run on the server. 
* Files stored in a “private” folder will only be accessible to code that’s executed on the server. These files will never be accessible to users.
* Files stored in a “public” folder are served to visitors. These are files like images, favicons, and the “robots.txt” file.
* Files stored in a “lib” folder are loaded before other files.

More info: http://meteortips.com/first-meteor-tutorial/structure/

## Deploy to Heroku using the the Meteor Buildpack Horse 

https://github.com/AdmitHub/meteor-buildpack-horse

To use this with your meteor app and heroku:

1. Set up your app to [deploy to heroku with git](https://devcenter.heroku.com/articles/git).
2. Set this repository as the buildpack URL:

        heroku buildpacks:set https://github.com/AdmitHub/meteor-buildpack-horse.git


3. Add the MongoLab addon (or whatever mongo provider you prefer)
        
        heroku addons:create mongolab


4. If it isn't set already, be sure to set the ``ROOT_URL`` for meteor (replace URL with whatever is appropriate):

        heroku config:set ROOT_URL=https://<yourapp>.herokuapp.com


5. Add [session affinity](https://devcenter.heroku.com/articles/session-affinity) so your app will still work with more than one dyno

        heroku labs:enable http-session-affinity


6. Optional step, if you are using a ```settings.json``` file to configure your Meteor application

         heroku config:add METEOR_SETTINGS="$(cat settings-production.json)"


7. Set the mail URL variable used for email notifications 

         heroku config:add MAIL_URL="smtp://AKIAI46OQOBM6QXZQJUQ:$STMP_PASSWORD@email-smtp.us-east-1.amazonaws.com:587"


8. Once that's done, you can deploy your app using this build pack any time by pushing to heroku:

    git push heroku master

### Extras

The basic buildpack should function correctly for any normal-ish meteor app,
with or without npm-container.  For extra steps needed for your particular build,
just add shell scripts to the `extra` folder and they will get sourced into the 
build.

Extras included in this version:
 - ``mongolab.sh``: Set ``MONGO_URL`` to the value of ``MONGOLAB_URI``.
