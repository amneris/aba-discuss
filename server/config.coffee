#
# PRIVATE FORUM CONFIGURATION
#
# telescope:invites should be DISABLED
Users.addField [
# Whether the user is invited or not
    fieldName: "telescope.isInvited",
    fieldSchema:
      type: Boolean,
      public: true,
      optional: true,
      editableBy: ["admin"],
      autoform:
        omit: true
]

Accounts.config
  restrictCreationByEmailDomain: 'abaenglish.com'

Telescope.callbacks.add "onCreateUser", (user) ->
  user.telescope.isInvited = true
  return user
