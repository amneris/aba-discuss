
Posts.addField({
    fieldName: 'url',
    fieldSchema: {
        type: String,
        optional: true,
        editableBy: ["admin"],
        autoform: {
            type: "bootstrap-url"
        }
    }
});

Posts.addField({
    fieldName: 'thumbnailUrl',
    fieldSchema: {
        type: String,
        optional: true,
        editableBy: ["admin"],
        autoform: {
            type: 'bootstrap-postthumbnail'
        }
    }
});