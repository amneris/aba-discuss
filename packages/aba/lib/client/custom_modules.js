
Telescope.modules.add("hero", [
  {
    template: 'aba_header',
    order: 10
  }
]);

Telescope.modules.removeAll("postThumbnail");
Telescope.modules.add("postThumbnail", [
  {
    template: 'aba_avatar',
    order: 10
  }
]);

Telescope.modules.removeAll("postComponents");

Telescope.modules.add("postComponents", [
  {
    template: 'post_content',
    order: 10
  },
  {
    template: 'post_upvote',
    order: 20
  },
  {
    template: 'post_discuss',
    order: 30
  }
]);

Telescope.modules.removeAll("postHeading");

Telescope.modules.add("postHeading", [
  {
    template: 'custom_post_title',
    order: 10
  },
  {
    template: 'post_info',
    order: 20
  },
]);

Telescope.modules.removeAll("postMeta");

Telescope.modules.add("postMeta", [
  {
    template: 'post_categories',
    order: 10
  },
  {
    template: 'aba-post-avatars',
    order: 20
  },
  // {
  //   template: 'post_comments_link',
  //   order: 30
  // },
  // {
  //   template: 'post_info',
  //   order: 40
  // },
  // {
  //   template: 'post_author',
  //   order: 50
  // },
  {
    template: 'post_admin',
    order: 60
  },
]);
