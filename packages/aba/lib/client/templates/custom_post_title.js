Template[getTemplate('custom_post_title')].helpers({
  // Generate link for post
  postLink: function(){
    return !!this.url ? getOutgoingUrl(this.url) : "/posts/"+this._id;
  },
  
  postTarget: function() {
    return !!this.url ? '_blank' : '';
  }
});
