Package.describe({
  summary: 'Aba custom theme',
  version: '0.1.0',
  name: 'aba'
});


Package.onUse(function (api) {

  // ---------------------------------- 1. Core dependency -----------------------------------

  api.use("telescope:core");
  api.use('telescope:theme-hubble');

  // ---------------------------------- 2. Files to include ----------------------------------

  // i18n config (must come first)

  api.addFiles([
    'package-tap.i18n'
  ], ['client', 'server']);

  // client & server

  // api.addFiles([
  //   'lib/custom_fields.js',
  //   'lib/template_modules.js',
  //   'lib/callbacks.js'
  // ], ['client', 'server']);

  // client

  api.addFiles([
    'lib/client/stylesheets/custom.scss',
    'lib/client/stylesheets/shared/_header.scss',
    'lib/client/stylesheets/shared/_fonts.scss',
    'lib/client/stylesheets/shared/_vars.scss',
    'lib/client/templates/custom_post_title.html',
    'lib/client/templates/aba_header.html',
    'lib/client/templates/aba_post_avatars.html',
    'lib/client/templates/aba_post_avatars.js',
    'lib/client/templates/aba_avatar.html',
    'lib/client/templates/custom_post_title.js',
    'lib/client/custom_templates.js',
    'lib/client/custom_modules.js',
    'lib/client/custom_form_fields.js'
  ], ['client']);

  // server

  // api.addFiles([
  //   'lib/server/autologin.js'
  // ], ['server', 'client']);

  // i18n languages (must come last)

  // api.addFiles([
  //   'lib/app.js'
  // ], ['client', 'server']);

});
